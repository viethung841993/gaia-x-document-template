
function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

window.onload = function() {
  let count = Array.from(document.querySelectorAll('a.merge-request')).map(e => e.href).filter(onlyUnique).length;
  let counter = document.createElement('span');
  counter.innerText = ' (' + count + ' open merge-requests)';
  document.querySelector('h1').insertAdjacentElement("beforeend", counter);

  // Working Draft - Non Definitive version Flag
  const main_div = document.querySelector("div[role='main']");
  const workingDraft_div = document.createElement('div');
  workingDraft_div.innerText = 'Working Draft - Non definitive version';
  workingDraft_div.classList.add('working_document_flag');
  main_div.prepend(workingDraft_div);
}
