#!/bin/bash
# set -ex Please don't uncomment it, for some reason it breaks the ci

FILE=${1:?No file was specified as first argument}

# Variables
MERMAID_REGEX_START='^```mermaid$'
MERMAID_REGEX_END='^```$'

LINE_COUNT=0
MERMAID_START=()
MERMAID_END=()

LINES_TO_REMOVE=0

MERMAID_BLOCK=0

# Get Mermaid graph starting and ending lines
while read -r LINE; do
    ((LINE_COUNT++))
    if [[ "${LINE}" =~ $MERMAID_REGEX_START ]]; then
        MERMAID_START+=( "$LINE_COUNT" )
        MERMAID_BLOCK=1
    fi
    if [[ "${LINE}" =~ $MERMAID_REGEX_END ]] && [[ $MERMAID_BLOCK == 1 ]]; then
        MERMAID_END+=( "$LINE_COUNT" )
        MERMAID_BLOCK=0
    fi
done < $FILE

# Convert mermaid graphs and replace them with png links
for ((i=0; i<${#MERMAID_START[@]}; i+=1)); do

    # Add LINES_TO_REMOVE
    START=$(( ${MERMAID_START[i]} - $LINES_TO_REMOVE))
    END=$(( ${MERMAID_END[i]} - $LINES_TO_REMOVE))

    # Get text from line a to line b
    GRAPH=$(sed -n "${START},${END}p" $FILE)
    echo "$GRAPH" > temp.md

    # Convert to png
    markdown_mermaid_to_images --file ./temp.md --output output/

    # Remove mermaid graph
    sed -i "${START},${END}d" $FILE

    # Insert png link
    PNG_LINK=$(head -n 1 output/temp.md)
    echo $PNG_LINK
    INSERT_AT=$(($START - 1))
    sed -i "$INSERT_AT a $PNG_LINK" $FILE
    
    # Remove graph in lines count
    DELTA=$(( ${MERMAID_END[i]} - ${MERMAID_START[i]} ))
    LINES_TO_REMOVE=$(( $LINES_TO_REMOVE + $DELTA)) 
done




